'use strict';

(function() {
    var neopalbot = angular.module('neopalbot', ['ngRoute', 'mm.foundation']);

    function ViewController($http, $location, $anchorScroll) {
        this.http = $http;
        this.location = $location;
        this.anchorScroll = $anchorScroll;

        this.updatePageContents();
    }

    ViewController.prototype.scrollTo = function(id) {
        console.log('scrolling!');
        this.location.hash(id);
        this.anchorScroll();
    };

    ViewController.prototype.updatePageContents = function() {
        this.http.get('api/preferences').then(function(response) {
            this.palbotPreferences = response.data;
            console.log('Preferences: ', this.palbotPreferences);
        }.bind(this));
        this.http.get('api/quotes').then(function(response) {
            this.palbotQuotes = response.data;
            console.log('Quotes: ', this.palbotQuotes);
        }.bind(this));
        this.http.get('api/appointments').then(function(response) {
            this.palbotAppointments = response.data;
            console.log('Appointments: ', this.palbotAppointments);
        }.bind(this));
        this.http.get('api/presets').then(function(response) {
            this.palbotPresets = response.data;
            console.log('Presets: ', this.palbotPresets);
        }.bind(this));
    };

    neopalbot.controller('ViewController', ViewController);
    neopalbot.config(function($routeProvider) {
        $routeProvider.when('/view', {
            templateUrl: 'snp/view.html',
            controller: 'ViewController as view'
        }).otherwise({
            redirectTo: '/view'
        });
    });
    neopalbot.run(function($rootScope, $location, $anchorScroll) {
        //when the route is changed scroll to the proper element.
        $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
            console.log('Scrolling!');
            if ($location.hash()) $anchorScroll();
        });
    });
})();
