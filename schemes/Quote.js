var QuoteScheme = {
    identifier: {
        type: String,
        index: {
            unique: true,
            dropDups: true
        }
    },
    quotes: [String]
};
module.exports = QuoteScheme;
