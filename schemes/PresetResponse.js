var PresetResponseScheme = {
    trigger: {
        type: String,
        index: {
            unique: true,
            dropDups: true
        }
    },
    action: Boolean,
    responses: [String]
};
module.exports = PresetResponseScheme;
