var AppointmentScheme = {
    identifier: {
        type: String,
        index: {
            unique: true,
            dropDups: true
        }
    },
    nextDue: Date,
    interval: Number
};
module.exports = AppointmentScheme;
