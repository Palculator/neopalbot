var BotPreferencesSchema = {
	globalCooldown: Number,
	userCooldown: Number,
	blockedUsers: [String]
};
module.exports = BotPreferencesSchema;
