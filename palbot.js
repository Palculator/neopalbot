function Palbot(botconf, winston, mongoose, q, irc, initFile) {
    this.botconf = botconf;
    this.winston = winston;
    this.mongoose = mongoose;
    this.q = q;
    this.irc = irc;

    this.BotPreferences = null;
    this.PresetResponse = null;
    this.Quote = null;
    this.Appointment = null;

    this.currentPreferences = null;

    var preferencesPromise = this.setupSchemes(initFile);

    this.handlers = [];
    this.initHandlers();

    this.irc.addListener('pm', this.handlePM.bind(this));
    this.login();
    if (this.botconf.channel) {
        this.channel = this.botconf.channel;
        this.irc.addListener('message' + this.botconf.channel, this.handleMessage.bind(this));
        var defer = this.q.defer();
        this.irc.join(this.botconf.channel, function() {
            defer.resolve();
        });
        defer.promise.done();
    }

    this.lastCommandDate = new Date(0);
    this.lastCommandDateBy = {};
}

Palbot.prototype.dropSchemeContents = function(Scheme) {
    Scheme.remove({}, function(err) {
        if (err) {
            this.winston.log('error', 'Error dropping scheme contents: ' + err);
        }
    }.bind(this));
};

Palbot.prototype.updateCurrentPreferences = function() {
    this.BotPreferences.find({}, function(err, preferences) {
        if (err) {
            this.winston.log('error', 'Error retrieving list of BotPreferences: %s', err);
        }

        for (var i = preferences.length - 1; i >= 0; i--) {
            this.currentPreferences = preferences[i];
            console.log('Found preferences: ');
            console.dir(this.currentPreferences);
            return;
        };
    }.bind(this));
};

Palbot.prototype.setupSchemes = function(initFile) {
    this.winston.log('db', 'Setting up database schemes.');

    var BotPreferencesSchema = new this.mongoose.Schema(require('./schemes/BotPreferences.js'));
    this.BotPreferences = this.mongoose.model('BotPreferences', BotPreferencesSchema);
    this.winston.log('db', 'Set up the BotPreferences schema.');

    var PresetResponseSchema = new this.mongoose.Schema(require('./schemes/PresetResponse.js'));
    this.PresetResponse = this.mongoose.model('PresetResponse', PresetResponseSchema);
    this.winston.log('db', 'Set up the preset response schema.');

    var QuoteSchema = new this.mongoose.Schema(require('./schemes/Quote.js'));
    this.Quote = this.mongoose.model('Quote', QuoteSchema);
    this.winston.log('db', 'Set up the Quote schema.');

    var AppointmentSchema = new this.mongoose.Schema(require('./schemes/Appointment.js'));
    this.Appointment = this.mongoose.model('Appointment', AppointmentSchema);
    this.winston.log('db', 'Set up the Appointment schema.');

    if (initFile) {
        this.winston.log('db', 'Dropping existing database contents for reinitialisation.');
        this.dropSchemeContents(this.BotPreferences);
        this.dropSchemeContents(this.PresetResponse);
        this.dropSchemeContents(this.Quote);
        this.dropSchemeContents(this.Appointment);

        var contents = require(initFile);
        var initPreferences = new this.BotPreferences(contents.initPreferences);
        initPreferences.save(function(err, preferences) {
            if (err) {
                this.winston.log('error', 'Error creating preferences: %s', err);
            }
            this.winston.log('db', 'Initialised bot preferences: %s', JSON.stringify(preferences));
            this.currentPreferences = preferences;
        }.bind(this));

        for (var trigger in contents.presetResponses) {
            if (contents.presetResponses.hasOwnProperty(trigger)) {
                var presetResponse = contents.presetResponses[trigger];
                this.newPresetResponse(trigger, presetResponse.action, presetResponse.responses);
            }
        }

        for (var character in contents.quotes) {
            if (contents.quotes.hasOwnProperty(character)) {
                var quotes = contents.quotes[character];
                this.newQuote(character, quotes);
            }
        }

        for (var identifier in contents.appointments) {
            if (contents.appointments.hasOwnProperty(identifier)) {
                var appointment = contents.appointments[identifier];
                this.newAppointment(identifier, Date.parse(appointment.nextDue));
            }
        }
    }

    this.updateCurrentPreferences();
};

Palbot.prototype.initHandlers = function() {
    this.handlers.push({
        pattern: /^q.*$/i,
        callback: this.handleQuote.bind(this)
    });
    this.handlers.push({
        pattern: /^bq.*$/i,
        callback: this.handleQuote.bind(this)
    });
    this.handlers.push({
        pattern: /^whens.*$/i,
        callback: this.handleWhens.bind(this)
    });
};

Palbot.prototype.handlePM = function(from, message) {
    this.winston.log('irc', 'Private message: <%s>: %s', from, message);
};

Palbot.prototype.handleMessage = function(from, message) {
    if (message[0] !== '!') {
        return;
    } else {
        message = message.substring(1).trim().toLowerCase();
    }

    var now = new Date();

    var delta = 0;
    if (this.lastCommandDateBy[from]) {
        delta = (now - this.lastCommandDateBy[from]) / 1000;
        if (delta < this.currentPreferences.userCooldown) {
            this.irc.notice(from, 'Your personal cooldown is still active for ' + (this.currentPreferences.userCooldown - delta) + ' second(s)');
            return;
        }
    }

    delta = (now - this.lastCommandDate) / 1000;
    if (delta < this.currentPreferences.globalCooldown) {
        this.irc.notice(from, 'Global cooldown is still active for ' + (this.currentPreferences.globalCooldown - delta) + ' second(s)');
        return;
    }

    for (var i = this.handlers.length - 1; i >= 0; i--) {
        var handler = this.handlers[i];
        if (message.match(handler.pattern)) {
            var successPromise = handler.callback(from, message);
            successPromise.then(function(success) {
                if (success) {
                    this.lastCommandDate = now;
                    this.lastCommandDateBy[from] = now;
                }
            }.bind(this));
            return;
        }
    };

    var presetPromise = this.handlePreset(from, message);
    presetPromise.then(function(found) {
        if (found) {
            this.lastCommandDate = now;
            this.lastCommandDateBy[from] = now;
        }
    }.bind(this));
};

Palbot.prototype.handlePreset = function(from, message) {
    var defer = this.q.defer();
    this.PresetResponse.find({}, function(err, results) {
        for (var i = results.length - 1; i >= 0; i--) {
            var result = results[i];
            if (message.toLowerCase().match(new RegExp('^' + result.trigger.trim().toLowerCase() + '.*', 'i'))) {
                var responses = result.responses;
                var response = responses[Math.floor(Math.random() * responses.length)];
                if (result.action) {
                    this.irc.action(this.channel, response);
                } else {
                    this.irc.say(this.channel, response);
                }
                defer.resolve(true);
                return;
            }
        }
        defer.resolve(false);
    }.bind(this));
    return defer.promise;
};

Palbot.prototype.extractIdentifier = function(message) {
    var space = message.indexOf(' ');
    message = message.substring(space).trim().toLowerCase();
    space = message.indexOf(' ');
    if (space != -1) {
        message = message.substring(0, space);
    }
    return message;
}

Palbot.prototype.normaliseIdentifier = function(identifier) {
    return identifier[0].toUpperCase() + identifier.substring(1);
};

Palbot.prototype.sendRandomQuote = function(result) {
    var quotes = result.quotes;
    var response = quotes[Math.floor(Math.random() * quotes.length)];
    this.irc.say(this.channel, this.normaliseIdentifier(result.identifier) + ': ' + response);
};

Palbot.prototype.handleQuote = function(from, message) {
    var defer = this.q.defer();
    message = this.extractIdentifier(message);
    this.Quote.find({
        identifier: message
    }, function(err, results) {
        if (results.length == 0) {
            this.Quote.find({}, function(err, results) {
                if (results.length == 0) {
                    defer.resolve(false);
                } else {
                    var result = results[Math.floor(Math.random() * results.length)];
                    this.sendRandomQuote(result);
                    defer.resolve(true);
                }
            }.bind(this));
            return;
        }
        for (var i = results.length - 1; i >= 0; i--) {
            var result = results[i];
            this.sendRandomQuote(result);
            defer.resolve(true);
            return;
        };
    }.bind(this));
    return defer.promise;
};

Palbot.prototype.formatNumberLength = function(num, length) {
    var r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
};

Palbot.prototype.formatDate = function(date) {
    return date.getFullYear() + '-' + this.formatNumberLength(date.getMonth() + 1, 2) + '-' + this.formatNumberLength(date.getDate(), 2) + ' ' + this.formatNumberLength(date.getHours(), 2) + ':' + this.formatNumberLength(date.getMinutes(), 2) + ':' + this.formatNumberLength(date.getSeconds(), 2);
};

Palbot.prototype.formatDelta = function(delta) {
    var days = Math.floor(delta / (3600 * 24));
    delta = delta % (3600 * 24);
    var hours = Math.floor(delta / 3600);
    delta = delta % 3600;
    var minutes = Math.floor(delta / 60);
    var seconds = delta % 60;
    var deltaString = '';
    var carry = false;
    if (days > 0) {
        deltaString += days + ' days';
        carry = true;
    }
    if (hours > 0) {
        if (carry) {
            deltaString += ', ';
        }
        deltaString += hours + ' hours';
        carry = true;
    }
    if (minutes > 0) {
        if (carry) {
            deltaString += ', ';
        }
        deltaString += minutes + ' minutes';
        carry = true;
    }
    if (seconds > 0) {
        if (carry) {
            deltaString += ', ';
        }
        deltaString += seconds + ' seconds';
    }
    return deltaString;
};

Palbot.prototype.postTimeDifference = function(appointment) {
    var now = new Date();
    var due = new Date(appointment.nextDue - (3600 * 2 * 1000));
    var date = this.formatDate(due);
    if (due < now) {
        this.irc.say(this.channel, '"' + appointment.identifier + '" already happened on ' + date);
        this.Appointment.remove({
            _id: appointment.id
        }, function(err) {
            this.winston.log('db', 'Removed obsolete appointment %s = [%s]', appointment.identifier, date);
        }.bind(this));
    } else {
        var delta = Math.floor((due - now) / 1000);
        var deltaString = this.formatDelta(delta);
        this.irc.say(this.channel, '"' + appointment.identifier + '" will be at ' + date + ', or in ' + deltaString);
    }
};

Palbot.prototype.handleWhens = function(from, message) {
    var defer = this.q.defer();
    message = this.extractIdentifier(message);
    this.Appointment.find({
        identifier: message
    }, function(err, results) {
        if (results.length == 0) {
            this.irc.notice(from, 'Unknown appointment: ' + message);
            defer.resolve(false);
        } else {
            this.postTimeDifference(results[0]);
            defer.resolve(true);
        }
    }.bind(this));
    return defer.promise;
};

Palbot.prototype.login = function() {
    this.winston.log('irc', 'Logging in with: %s', 'AUTH ' + this.botconf.accountName + ' ' + this.botconf.accountPassword);
    this.irc.say(this.botconf.nickServ, 'AUTH ' + this.botconf.accountName + ' ' + this.botconf.accountPassword);
    this.winston.log('irc', 'Logged in to account %s', this.botconf.accountName);
    // this.irc.say(this.botconf.nickServ, 'GHOST ' + this.botconf.accountName);
    // this.irc.say(this.botconf.nickServ, 'RECLAIM ' + this.botconf.accountName);
};

Palbot.prototype.newPresetResponse = function(trigger, action, responses, callback) {
    var presetResponse = new this.PresetResponse({
        trigger: trigger,
        action: action,
        responses: responses
    });
    presetResponse.save(function(err, response) {
        if (err) {
            this.winston.log('error', 'Error saving preset response: %s', err);
            callback(err, null);
            return;
        }

        this.winston.log('db', 'Created preset response %s = [%d], id: %s', response.trigger, response.responses.length, response.id);
        if (callback) {
            callback(null, response);
        }
    }.bind(this));
};

Palbot.prototype.setPresetResponseAction = function(id, action, callback) {
    this.PresetResponse.update({
        _id: id
    }, {
        action: action
    }, function(err, affected) {
        if (err) {
            this.winston.log('error', 'Error setting PresetResponse action %s: %s', id, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Set the action flag of %s to %s.', id, action);
        if (callback) {
            callback(null, affected);
        }
    }.bind(this));
};

Palbot.prototype.getPresetResponse = function(id, callback) {
    this.PresetResponse.findById(id, function(err, response) {
        if (err) {
            this.winston.log('error', 'Error fetching PresetResponse %s: %s', id, err)
            if (callback) {
                callback(err);
            }
            return;
        }

        if (callback) {
            callback(null, response);
        }
    }.bind(this));
};

Palbot.prototype.getPresetResponses = function(callback) {
    this.PresetResponse.find({}, function(err, responses) {
        if (err) {
            this.winston.log('error', 'Error fetching PresetResponses: %s', err);
            if (callback) {
                callback(err);
            }
            return;
        }

        if (callback) {
            callback(null, responses);
        }
    }.bind(this));
};

Palbot.prototype.removePresetResponse = function(id, callback) {
    this.PresetResponse.remove({
        _id: id
    }, function(err, affected) {
        if (err) {
            this.winston.log('error', 'Error removing PresetResponse %s: %s', id, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Removed the PresetResponse %s.', id);
        if (callback) {
            callback(null, affected);
        }
    }.bind(this));
};

Palbot.prototype.addPresetResponseOption = function(id, toadd, callback) {
    this.PresetResponse.update({
        _id: id
    }, {
        $push: {
            responses: toadd
        }
    }, function(err, affected) {
        if (err) {
            this.winston.log('error', 'Error pushing new response option %s + "%s": %s', id, toadd, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Pushed an option to the PresetResponse %s: "%s"', id, toadd);
        if (callback) {
            callback(null, affected);
        }
    }.bind(this));
};

Palbot.prototype.removePresetResponseOption = function(id, toremove, callback) {
    this.PresetResponse.update({
        _id: id,
    }, {
        $pull: {
            responses: toremove
        }
    }, function(err, affected) {
        if (err) {
            this.winston.log('error', 'Error pulling new response option: %s - "%s": %s', id, toremove, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Pulled an option from the PresetResponse %s: "%s"', id, toremove);
        if (callback) {
            callback(null, affected);
        }
    }.bind(this));
};

Palbot.prototype.newQuote = function(identifier, quotes, callback) {
    var quote = new this.Quote({
        identifier: identifier,
        quotes: quotes
    });
    quote.save(function(err, quote) {
        if (err) {
            this.winston.log('error', 'Error saving quote: %s = [%d]: %s', identifier, quotes.length, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Created new quote %s = [%d]', identifier, quotes.length);
        if (callback) {
            callback(null, quote);
        }
    }.bind(this));
};

Palbot.prototype.getQuote = function(id, callback) {
    this.Quote.findById(id,
        function(err, quote) {
            if (err) {
                this.winston.log('error', 'Error fetching quote %s: %s', id, err);
                if (callback) {
                    callback(err);
                }
                return;
            }

            if (callback) {
                callback(null, quote);
            }
        }.bind(this));
};

Palbot.prototype.getQuotes = function(callback) {
    this.Quote.find({}, function(err, results) {
        if (err) {
            this.winston.log('error', 'Error fetching all quotes: %s', err);
            if (callback) {
                callback(err);
            }
            return;
        }

        if (callback) {
            callback(null, results);
        }
    }.bind(this));
};

Palbot.prototype.removeQuote = function(id, callback) {
    this.Quote.remove({
        _id: id
    }, function(err) {
        if (err) {
            this.winston.log('error', 'Error removing quote %s: %s', id, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Removed quote %s.', id);
        if (callback) {
            callback();
        }
    }.bind(this));
};

Palbot.prototype.addQuoteOption = function(id, toadd, callback) {
    this.Quote.update({
        _id: id
    }, {
        $push: {
            quotes: toadd
        }
    }, function(err) {
        if (err) {
            this.winston.log('error', 'Error adding quote option %s + %s: %s', id, toadd, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Pushed new quote option %s: "%s"', id, toadd);
        if (callback) {
            callback();
        }
    }.bind(this));
};

Palbot.prototype.removeQuoteOption = function(id, toremove, callback) {
    this.Quote.update({
        _id: id
    }, {
        $pull: {
            quotes: toremove
        }
    }, function(err) {
        if (err) {
            this.winston.log('error', 'Error removing quote option %s + %s: %s', id, toremove, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Pulled a quote option %s: "%s"', id, toremove);
        if (callback) {
            callback();
        }
    }.bind(this));
};

Palbot.prototype.newAppointment = function(identifier, nextDue, callback) {
    var appointment = new this.Appointment({
        identifier: identifier,
        nextDue: nextDue
    });
    appointment.save(function(err, appointment) {
        if (err) {
            this.winston.log('error', 'Error creating new appointment %s = %s: %s', identifier, nextDue.toISOString(), err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Created new appointment %s = %s', appointment.identifier, appointment.nextDue.toISOString());
        if (callback) {
            callback(appointment);
        }
    }.bind(this));
};

Palbot.prototype.setAppointmentDate = function(id, nextDue, callback) {
    this.Appointment.update({
        _id: id
    }, {
        nextDue: nextDue
    }, function(err) {
        if (err) {
            this.winston.log('error', 'Error setting appointment date of %s = %s: %s', id, nextDue.toISOString(), err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Set the due date of %s = %s', id, nextDue.toISOString());
        if (callback) {
            callback();
        }
    }.bind(this));
};

Palbot.prototype.getAppointment = function(id, callback) {
    this.Appointment.findById(id, function(err, appointment) {
        if (err) {
            this.winston.log('error', 'Error getting appointment %s: %s', id, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        if (callback) {
            callback(null, appointment);
        }
    }.bind(this));
};

Palbot.prototype.getAppointments = function(callback) {
    this.Appointment.find({}, function(err, appointments) {
        if (err) {
            this.winston.log('error', 'Error fetching all appointments: %s', err);
            if (callback) {
                callback(err);
            }
            return;
        }

        if (callback) {
            callback(null, appointments);
        }
    }.bind(this));
};

Palbot.prototype.removeAppointment = function(id, callback) {
    this.Appointment.remove({
        _id: id
    }, function(err) {
        if (err) {
            this.winston.log('error', 'Error removing appointment %s: %s', id, err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.winston.log('db', 'Removed appointment %s.', id);
        if (callback) {
            callback();
        }
    }.bind(this));
};

Palbot.prototype.setPreferences = function(preferences, callback) {
    this.BotPreferences.update({
        _id: this.currentPreferences.id
    }, preferences, function(err) {
        if (err) {
            this.winston('error', 'Error setting preferences %s: ', JSON.stringify(preferences), err);
            if (callback) {
                callback(err);
            }
            return;
        }

        this.updateCurrentPreferences();
        if (callback) {
            callback(null, this.currentPreferences);
        }
    }.bind(this));
};

module.exports = Palbot;
