var winston = require('winston');
var q = require('q');
var botconf = require('./botconf.json');

function setupLogging() {
    var logFile = 'palbot.log';
    winston.add(winston.transports.File, {
        filename: logFile
    });
    var palbotLogLevels = {
        levels: {
            info: 0,
            debug: 1,
            verbose: 2,
            irc: 3,
            api: 4,
            http: 5,
            error: 6,
            db: 7
        },
        colors: {
            info: 'blue',
            debug: 'green',
            verbose: 'white',
            error: 'red',
            irc: 'grey',
            api: 'orange',
            http: 'yellow',
            db: 'pink'
        }
    };
    winston.setLevels(palbotLogLevels.levels);
    winston.addColors(palbotLogLevels.colors);
    winston.log('info', 'Started the Winston logging library with logFile: %s', logFile);
}

function setupServer(defer, palbot) {
    var express = require('express');
    var path = require('path');
    var logger = require('morgan');
    var cookieParser = require('cookie-parser');
    var bodyParser = require('body-parser');

    var app = express();

    // app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));

    var router = express.Router();
    router.use('/preferences', require('./routes/BotPreferencesRoute.js')(palbot));
    router.use('/presets', require('./routes/PresetResponsesRoute.js')(palbot));
    router.use('/quotes', require('./routes/QuotesRoute.js')(palbot));
    router.use('/appointments', require('./routes/AppointmentsRoute.js')(palbot));
    app.use('/api', router);

    defer.resolve(app);
}

function setupPalbot(defer, mongoose, client) {
    var Palbot = require('./palbot.js');
    var initFile = null;
    if (process.argv[2]) {
        initFile = process.argv[2];
    }
    var bot = new Palbot(botconf, winston, mongoose, q, client, initFile);
    setupServer(defer, bot);
}

function setupIRC(defer, mongoose) {
    winston.log('irc', 'Initialising the IRC client library.');
    var irc = require('irc');
    var client = new irc.Client(botconf.server, botconf.nick, botconf.options);
    winston.log('irc', 'Connecting to server %s as %s...', botconf.server, botconf.nick);
    client.connect(5, function() {
        winston.log('irc', 'Connected.');
        setupPalbot(defer, mongoose, client)
    });
}

function setupMongoose(defer) {
    winston.log('debug', 'Initialising Mongoose database connection and schemes.');
    var mongoose = require('mongoose');

    var db = mongoose.connection;
    db.on('error', function(err) {
        winston.log('error', 'Error on Mongoose database: ', err);
    });

    db.once('open', function() {
        winston.log('db', 'Connected to the MongoDB. Setting up schemes.');
        setupIRC(defer, mongoose);
    });

    var mongoLocation = botconf.mongoLocation;
    winston.log('db', 'Connecting to the MongoDB at %s', mongoLocation);
    mongoose.connect(mongoLocation);
}

setupLogging();
var defer = q.defer();
setupMongoose(defer);
module.exports = defer.promise;
