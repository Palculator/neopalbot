function setupRoute(palbot) {
    var express = require('express');
    var router = express.Router();

    router.route('/').get(function(req, res) {
        palbot.getQuotes(function(err, quotes) {
            if (err) {
                res.send(err);
                return;
            }

            res.json(quotes);
        });
    }).put(function(req, res) {
        var quote = res.body;
        if (typeof quote.identifier !== 'undefined') {
            palbot.newQuote(quote.identifier, quote.quotes, function(err, quote) {
                if (err) {
                    res.send(err);
                    return;
                }

                res.json(quote);
            });
        } else {
            res.status(400).send('Need to specify at least an identifier.');
        }
    });

    router.route('/:quoteId').get(function(req, res) {
        var id = req.params.quoteId;

        palbot.getQuote(id, function(err, quote) {
            if (err) {
                res.send(err);
                return;
            }

            if (quote) {
                res.json(quote);
            } else {
                res.status(404).send('No quote with id = ' + id);
            }
        });
    }).delete(function(req, res) {
        var id = req.params.quoteId;

        palbot.removeQuote(id, function(err, affected) {
            if (err) {
                res.send(err);
                return;
            }

            res.json(affected);
        });
    });

    router.route('/:quoteId/quotes').get(function(req, res) {
        var id = req.params.quoteId;

        palbot.getQuote(id, function(err, quote) {
            if (err) {
                res.send(err);
                return;
            }

            if (quote) {
                res.json(quote.quotes);
            } else {
                res.status(404).send('No quote with id = ' + id);
            }
        });
    }).put(function(req, res) {
        var id = req.params.quoteId;
        var toadd = req.body.toadd;

        palbot.addQuoteOption(id, toadd, function(err, affected) {
            if (err) {
                res.send(err);
                return;
            }

            res.json(affected);
        });
    }).delete(function(req, res) {
        var id = req.params.quoteId;
        var toremove = req.body.toremove;

        palbot.removeQuoteOption(id, toremove, function(err, affected) {
            if (err) {
                res.send(err);
                return;
            }

            res.json(affected);
        });
    });

    return router;
}

module.exports = setupRoute;
