function setupRoutes(palbot) {
    var express = require('express');
    var router = express.Router();
    var BotPreferences = palbot.BotPreferences;

    router.route('/').get(function(req, res) {
        res.json(palbot.currentPreferences);
    }).post(function(req, res) {
        var preferences = {
            globalCooldown: req.body.globalCooldown,
            userCooldown: req.body.userCooldown
        };
        palbot.setPreferences(preferences, function(err, preferences) {
            if (err) {
                res.send(err);
                return;
            }

            res.json(preferences);
        });
    });
    return router;
};
module.exports = setupRoutes;
