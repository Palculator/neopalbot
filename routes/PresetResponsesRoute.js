function setupRoutes(palbot) {
    var express = require('express');
    var router = express.Router();

    router.route('/').get(function(req, res) {
        palbot.getPresetResponses(function(err, responses) {
            if (err) {
                res.send(err);
                return;
            }

            res.json(responses);
        });
    }).put(function(req, res) {
        var response = req.body;
        if (typeof response.trigger !== 'undefined' && typeof response.action !== 'undefined') {
            palbot.newPresetResponse(response.trigger, response.action, response.responses, function(err, response) {
                if (err) {
                    res.send(err);
                    return;
                }

                res.json(response);
            });
        } else {
            res.status(400).send({
                error: 'Need to define at least the trigger and action.'
            });
        }
    });

    router.route('/:responseId').get(function(req, res) {
        palbot.getPresetResponse(req.params.responseId, function(err, response) {
            if (err) {
                res.send(err);
                return;
            }

            if (response) {
                res.json(response);
            } else {
                res.status(404).send('No response with id = ' + id);
            }
        });
    }).post(function(req, res) {
        var id = req.params.responseId;
        var action = req.body.action;

        palbot.setPresetResponseAction(id, action, function(err, affected) {
            if (err) {
                res.send(err);
                return;
            }

            res.json({
                affected: affected
            });
        });
    }).delete(function(req, res) {
        var id = req.params.responseId;

        palbot.removePresetResponse(id, function(err, affected) {
            if (err) {
                res.send(err);
                return;
            }

            res.json({
                affected: affected
            });
        });
    });

    router.route('/:responseId/responses').get(function(req, res) {
        var id = req.params.responseId;

        palbot.getPresetResponse(id, function(err, response) {
            if (err) {
                res.send(err);
                return;
            }

            if (response) {
                res.json(response.responses);
            } else {
                res.status(404).send('No response with id = ' + id);
            }
        });
    }).put(function(req, res) {
        var id = req.params.responseId;
        var toadd = req.body.toadd;

        palbot.addPresetResponseOption(id, toadd, function(err, affected) {
            if (err) {
                res.send(err);
                return;
            }

            res.json({
                affected: affected
            });
        });
    }).delete(function(req, res) {
        var id = req.params.responseId;
        var toremove = req.body.toremove;

        palbot.removePresetResponseOption(id, toremove, function(err, affected) {
            if (err) {
                res.send(err);
                return;
            }

            res.json({
                affected: affected
            });
        });
    });

    return router;
}

module.exports = setupRoutes;
