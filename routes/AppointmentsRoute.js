function setupRoute(palbot) {
    var express = require('express');
    var router = express.Router();

    router.route('/').get(function(req, res) {
        palbot.getAppointments(function(err, appointments) {
            if (err) {
                res.send(err);
                return;
            }

            res.json(appointments);
        });
    }).put(function(req, res) {
        var appointment = req.body;
        if (typeof appointment.identifier !== 'undefined') {
            palbot.newAppointment(appointment.identifier, Date.parse(appointment.nextDue), function(err, appointment) {
                if (err) {
                    res.send(err);
                    return;
                }

                res.json(appointment);
            });
        } else {
            res.status(400).send('Need to specify at least an identifier.');
        }
    });

    router.route('/:appointmentId').get(function(req, res) {
        var id = req.params.appointmentId;

        palbot.getAppointment(id, function(err, appointment) {
            if (err) {
                res.send(err);
                return;
            }

            if (appointment) {
                res.json(appointment);
            } else {
                res.status(404).send('No appointment with id = ' + id);
            }
        });
    }).post(function(req, res) {
        var id = req.params.appointmentId;
        var nextDue = Date.parse(req.body.nextDue);

        palbot.setAppointmentDate(id, nextDue, function(err, affected) {
            if (err) {
                res.send(err);
                return;
            }

            res.json(affected);
        });
    }).delete(function(req, res) {
        var id = req.params.appointmentId;

        palbot.removeAppointment(id, function(err, affected) {
            if (err) {
                res.send(err);
                return;
            }

            res.json(affected);
        });
    });

    return router;
}

module.exports = setupRoute;
